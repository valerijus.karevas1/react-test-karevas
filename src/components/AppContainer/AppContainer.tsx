import React, { FunctionComponent } from 'react';
import { Form, Col, Button, FormLabel } from 'react-bootstrap';
import './AppContainer.css';

interface IReceipt {
    name: string,
    id: string,
    total: number,
    expences: Array<IExpences>
}

interface IExpences {
    id: string
    name: string,
    price: number
}

interface ICategory {
    description: string
    value: string
}

export const AppContainer: FunctionComponent = () => {
    const [receipts, setReceipts] = React.useState<Array<IReceipt>>([]);
    const [mainTotal, setMainTotal] = React.useState(0);
    const [categories] = React.useState<Array<ICategory>>([
        {
            description: 'Groceries',
            value: 'GROC'
        },
        {
            description: 'Entertainment',
            value: 'ENT'
        }
    ]);

    const addExpence = (e: any) => {
        const tempReceipts = [...receipts]
        var receiptId = e.target.id;
        for (const row of tempReceipts) {
            if (row.id === receiptId) {
                row.expences.push(
                    {
                        id: 'expence-' + new Date().getTime(),
                        name: '',
                        price: 0
                    }
                )
            }
        }
        setTotals(tempReceipts);
    };

    const addReceipt = () => {
        const tempReceipts = [
            ...receipts,
            {
                name: '',
                id: 'receipt-' + new Date().getTime(),
                total: 0,
                expences: []
            }]
        setReceipts(tempReceipts);
    };

    const setTotals = (tempReceipts: any) => {
        for (const row of tempReceipts) {
            let recTotal = 0;
            for (const exp of row.expences) {
                let value = !isNaN(parseFloat(exp['price'])) ? exp['price'] : 0;
                recTotal = value + recTotal
            }
            row.total = recTotal;
        }
        setReceipts(tempReceipts);
        setMainTotal(getMainTotal(tempReceipts));
    }

    const handleChange = (e: any) => {
        let value = parseFloat(e.target.value);
        const id = e.target.id;
        const tempReceipts = [...receipts]
        for (const row of tempReceipts) {
            for (const exp of row.expences) {
                if (exp.id === id) {
                    exp.price = value;
                }
            }
        }
        setTotals(tempReceipts);
    }

    const getMainTotal = (tempReceipts: any) => {
        let total = 0;
        for (const row of tempReceipts) {
            total = total + row.total;
        }
        return total;
    }

    return (
        <div>
            {
                receipts.map(function (item, i) {
                    return <Form key={i} className='upperRow'>
                        <Form.Row>
                            <Col>
                                <Form.Group>
                                    <Form.Control as="select">
                                        {
                                            categories.map(function (catItem, c) {
                                                return <option key={c} value={catItem.value}>
                                                    {catItem.description}
                                                </option>
                                            })
                                        }
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col sm="auto">
                                <Button
                                    variant="primary"
                                    id={item.id}
                                    onClick={addExpence}>
                                    Add Expence</Button>
                            </Col>
                        </Form.Row>

                        {
                            item.expences.map(function (expItem, j) {
                                return <Form.Row key={j} className="expenceRow">
                                    <Col sm={10}>
                                        <Form.Control
                                            value={expItem.name}
                                            placeholder='Name'
                                        />
                                    </Col>
                                    <Col sm={2}>
                                        <Form.Control
                                            placeholder='Price'
                                            type="number"
                                            id={expItem.id}
                                            value={expItem.price.toString()}
                                            onInput={handleChange}
                                        />
                                    </Col>
                                </Form.Row>
                            })
                        }

                        <Form.Row>
                            <Col sm={10} className='expenceTotalRowText'>
                                <FormLabel >
                                    Total:
                                </FormLabel>
                            </Col>
                            <Col sm={2} >
                                <FormLabel className='expenceTotalRowValue'>
                                    {item.total} €
                                </FormLabel>
                            </Col>
                        </Form.Row>
                    </Form>
                })
            }
            <Form >
                <Form.Row className='totalRow'>
                    <Col>
                        <FormLabel className='totalPrice'>
                            <b>Total: {mainTotal} €</b>
                        </FormLabel>
                    </Col>
                    <Col sm="auto">
                        <Button variant="primary" onClick={addReceipt}>Add Receipt</Button>
                    </Col>
                </Form.Row>
            </Form>
        </div>
    );
}

