import React from 'react';
import { AppContainer } from '../AppContainer/AppContainer'
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <AppContainer/>
    </div>
  );
}

export default App;
